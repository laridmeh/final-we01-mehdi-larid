ligne directrice: Fonctionnement d'internet et du web

### Commentaire de la proposition de Stéphane Bortzmeyer

Dans son article publié dans Framablog [Le Web est-il devenu trop compliqué ?](https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique), Stéphane Bortzmeyer commence par pointer des problèmes réels du Web d'aujourd'hui :

1. Les barrières à l'entrée du secteur du Web;
2. La monopolisation par les acteurs déjà en places;
3. Le fait que le web serait *designer* à 50% pour l'utilisateur et à 50% pour les publicitaires;

Après avoir pointé ces problèmes il énonce une solution, le projet Gemini.

#### Les barrières à l'entrée du secteur du Web

On défini en économie la barrière à l'entrée d'un secteur économique comme les problèmes qu'un potentiel nouvel acteur va rencontré s'il voulait commencé une activité dans ce dit domaine.

Il affirme que la complexité du Web ne touche pas que les développeurs comme ce que certains pourraient dire d'après lui (présence d'une citation). On pourrait dire, il est vrai, que la complexité de fonctionnement du Web est "cachée" à l'utilisateur lambda. En effet, de nos jours quasiment tous les utilisateurs du Web de par le monde utilisent des interfaces graphiques et des navigateurs qui s'occupent d'effectuer les calculs, requêtes et actions qui paraîtrons complexes à ceux qui n'en n'ont pas l'habitude. Rien que la surface des interactions réseaux, l'utilisateur n'est pas au courant que pour accéder à la page Web qu'il désire, son navigateur a du construire et envoyer une requête HTTP qui a du passé de l'ordinateur au routeur jusqu'à (après plusieurs passages chez des acteurs de différentes tailles comme des fournisseurs d'accès à Internet par exemple) arriver au serveur de la page Web qui va lui même construire et renvoyé sa réponse.

#### La monopolisation par les acteurs déjà en places

D'après Bortzmeyer, c'est à cause de cette complexité logiciel que peu d'acteurs peuvent apporté une vrai concurrence. J'ajouterai cependant qu'il y a d'autres raisons comme le potentiel contrôle des résultats par les moteurs de recherches, par exemple si un nouvel acteur souhaite lancé son propre navigateur Web, outre les problèmes techniques indiscutables, ce navigateur sera accessible via un site Web depuis un serveur possédant un nom de domaine unique permettant à l'humain de le comprendre (plutôt qu'une adresse IP de la forme ??.??.??.??). 

Dans la logique d'Internet à l'heure actuel, ce site serait alors indexé par les différents moteurs de recherche et apparaîtra plus ou moins haut dans les résultats des recherches utilisateurs. Mais qui décide de la priorité du site de ce nouveau navigateur dans les résultats ? Dans l'exemple de Google il préférera laissé la priorité à Google Chrome ou encore à Edge ce qui est un problème pour une concurrence saine. Malgré tout, il est nécessaire qu'il y ait de nouveaux acteurs qui entrent dans le secteur afin d'avoir plus de **choix** et donc espérer que cette situation s'améliore.

> Chrome, Edge et Safari utilisent le même moteur de rendu, WebKit 

Cet exemple vient appuyer l'idée qu'il y a sûrement plus qu'un simple manque technique pour le manque de concurrence, depuis quelques années énormément de startup et petites entreprises se lancent et révolutionne petit par petit le Web et même le numérique en règle général (Cf Mouvement low-tech) il est alors étrange que personne n'ait réussi à faire naître une concurrence dans les moteurs de rendu pour navigateur Web. On peut alors avancé l'hypothèse que j'ai sous-estimé l'importance de la taille d'une entreprise dans un tel projet, peut être que ce genre de projet nécessite effectivement une grande quantité de personne afin de voir le jour.

#### Le Web avantage autant les publicitaires que les utilisateurs

Il est vrai que depuis quelques années la taille des sites Web à énormément augmenté, d'environ 400% en 10 ans, est les deux principales raisons pour cela sont sans doute les scripts de publicité, de récolte de vos informations personnels et l'augmentation croissante des standards de qualité visuels des sites Web.

#### Projet Gemini

D'après Stéphane Bortzmeyer, le projet Gemini est une approche radicale aux problèmes qu'il a énoncé auparavant. Il ne s'agit plus de réparer les erreurs commises par le Web mais de le remplacer totalement. Gemini reprend quelques méthodes du web en simplifiant le plus de sujet technique possible (communication entre le serveur et le navigateur, pas de cookies ou de prises d'information non nécessaire, non extensible, non améliorable par ajout de fonctions, sans même d'image). Ces choix peuvent paraître surprenant mais assure vraiment que la complexité technique ne sera pas un frein pour les entreprises et que ce produit est tourné vers l'utilisateur. Moins de confort visuels pour plus de confort intellectuel sur le long terme.

Un internaute ayant fait savoir à l'auteur que les solutions de même ordre que le projet Gemini comme le navigateur dyllo n'était pas assez mis à jour et donc ne pouvait pas suivre l'évolution de tous les sites Web ce à quoi l'auteur à répondu que le simple fait qu'un navigateur pouvait "périmer" (c'est mon propre mot) était synonyme du problème du Web à l'heure actuel et qu'il ne faut absolument pas que cela reste normal dans les esprits des utilisateurs. Les langages de création de contenu sur le Web (HTML pour la forme, CSS pour le style et JavaScript pour les scripts) n'ont pas fondamentalement changés depuis des années ce qui devrait permettre une certaine stabilité pour le navigateur.

Commentaire écrit et édité par Mehdi LARID